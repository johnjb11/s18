console.log("Hello World")
/*let trainer = {};
console.log(trainer);*/
let trainer2 = {
	name: "Eminem",
	gender: "male",
	health: "100",
	action: function(){
		console.log("Mom's spaghetti is the best!")
	}
};

console.log(trainer2.name);
console.log(trainer2['health']);
trainer2.action();

function trainer (name, gender, health){
	this.name = name;
	this.gender = gender;
	this.health = health;
}


function pokemon(name){
	this.name = name;
	this.health = 100;
	this.tackle= function(opponent){
			console.log(`${this.name} tackled ${opponent.name}`);
			opponent.health -= 30;
			console.log(`${opponent.name}'s health is now ${opponent.health}`);
	}
}

let pikpik = new pokemon("pikpik");
let ratrat = new pokemon("ratrat");

pikpik.tackle(ratrat);